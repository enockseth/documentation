# Post-Install

## Dual-boot

If you setup a dual-boot, make sure that you install os-prober.

## Installing Software

Generally searching for "Software" or "Program" on the Beginner Distros will pull up a nice interface for you to search for and install packages. For more see [here](../Linux/Components/package-manager.md).

## Updating

In general, search for "Update". Some distros have a nice interface for updates. If not, see [here](../Linux/Components/package-manager.md).

## Security

-   With Windows you needed an Anti-Virus program, but not in Linux. (see [here](../Linux/Security/antivirus.md) if you want one anyway). However, it is recommended that you have a firewall to block other computers on the network from making connections into your machine. 

-   To set up a firewall:

    1.  Install the package gufw. 

    2.  Open gufw

    3.  Click on the status slider to turn it on.

    
