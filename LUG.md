# Connect With Us!
## Meetings
Weekly on Tuesdays at 6pm on Jitsi ([link](https://jitsi.osulug.club/WeeklyLUG)) Join one of our social platforms for the meeting password.
## Mailing List
Join our mailing list [here](http://lists.oregonstate.edu/mailman/listinfo/linux)
## Communication
Matrix is our primary communication method, however Discord and IRC are [bridged](https://matrix.org/bridges), so feel free to join whichever you feel like.
### Matrix
- Go to [app.element.io](https://app.element.io)
- Create an account if you do not already have an account on a Matrix server
- Join our [community](https://matrix.to/#/+osulug:osulug.club)
- Join our channels at `#general:osulug.club` and `#announcements:osulug.club`
### Discord
- [Download](https://discord.com/download) discord (or run in [browser](https://discord.com))
- Join a server by hitting the plus in the lower left hand corner
- Join our server with the invite link: `rwNeYB2`
### IRC
- Open an IRC Client (ex. [freenode's in-browser](https://webchat.freenode.net/))
- Put in a unique nickname and go to the channel `osu-lug`
- Register your nickname by following the instructions [here](https://freenode.net/kb/answer/registration)
More comprehensive documentation found [here](https://lug.oregonstate.edu/blog/irc/)
