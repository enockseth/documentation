---
name: Welcome
---

# Welcome
Welcome to Oregon State University Linux Users Group documentation on Linux. If you find any errors, or wish to help expand this documentation please see [contributing.md](contributing.md)
## Goals of This Documentation
- **Inform** newcomers can figure out how to get started.
- **Guide** newcomers can get an overview of what Linux is, as well as some areas in which they may want to learn more.
- **Showcase** useful applications that may be otherwise hard to find.
- **Reference** users to help them understand how the components operate together.
- **Refer** readers to existing external documentation when possible, but not being afraid of explaining if no such useful documentation exists.
- **Learn** from each other

## Using This Documentation
Generally the more basic topics will be listed first. Feel free to skip around.
### Sections
#### [Home](https://docs.lug.oregonstate.edu/)
This has general information about how to get started. As well as how to get [connected](LUG.md) and involved with the club. Highly recommended that you read these.
#### [Install](https://docs.lug.oregonstate.edu/Install/general/)
How to install various distributions of Linux, in various different ways (bare metal install, dual-boot, virtualized).
#### [Applications](https://docs.lug.oregonstate.edu/Applications/general/)
Collection of applications, such as word processors, audio editors, media viewers, ect.
#### [Tools](https://docs.lug.oregonstate.edu/Tools/storage/)
Similar to applications, but with more of a utility goal. Such as ping, dig, ncdu, ect.
#### [Linux](https://docs.lug.oregonstate.edu/Linux/basic-commands/)
Covers a wide range of Linux related topics. Such as how to use bash, components of Linux, virtualization, distributions, backups, ect.
#### [History](https://docs.lug.oregonstate.edu/History/Unix/)
Some history on how Open Source went from nothing but a dream, to the widespread phenomena it is today.
#### [OSU](https://docs.lug.oregonstate.edu/OSU/Classes/cs271/)
Helpful information about using OSU services on Linux, as well as any notes on using Linux in specific classes.

## Terminology
- **LUG** - Linux Users Group

- **distro/distribution** - a particular packaged configuration and software with Linux. [More detail](https://itsfoss.com/what-is-linux-distribution/)

## Thank You
Huge shout out to the amazing people at the [Open Source Lab](https://osuosl.org) who are providing the resources for hosting.
