# Contributing
## Documentation
1. Fork [Our Gitlab repo](https://gitlab.com/osu-lug/projects/documentation) 
2. Make your changes
3. Make a PR (See [Git](Tools/git.md) for help using Git)
## Markdown
See [here](./Linux/programming-langs.md#markdown) for reference.
### Markdown Standards
Although we are using mkdocs, and that does support markdown extensions, only use standard markdown, so these documents can be portable.
### Including
- Headers
- Lists (both numbered and unnumbered)
- Fenced Code blocks (triple backtick)
- Command (single backtick)
- Images
- Links
- Emphasis (Bold, Italics, Underline, Strike-Through)
### Excluding
- Horizontal Rules (use headers instead) (if you do find a reason to use these, use `***` and specify in the PR why it was needed)
- Tables (not standardized) (if you find a case to need these, feel free to do so in the PR)
- Other fancy elements
### Spacing Standards
- Do not enforce a max width.
- Space after the \# in a header before the text.
### Table Of Contents
mkdocs only generates a Table of contents if you have one top level header.