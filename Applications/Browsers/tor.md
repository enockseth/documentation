# Tor
Tor Browser is a hardened version of [Firefox](./firefox.md) Designed to make you more anonymous when browsing online (see [here](https://2019.www.torproject.org/docs/faq.html.en#AmITotallyAnonymous) and [here](https://2019.www.torproject.org/docs/faq.html.en#WhatProtectionsDoesTorProvide) for disclaimers). Read the disclaimers and understand how Tor works before using it for anonymity. Don't use Tor for illegal activities ([abuse FAQ](https://2019.www.torproject.org/docs/faq-abuse.html.en)).

<https://www.torproject.org/>

[User Manual](https://tb-manual.torproject.org)

[Gitlab documentation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/gitlab/)

[Old website](https://2019.www.torproject.org/index.html.en)