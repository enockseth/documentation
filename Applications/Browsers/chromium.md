# Chromium
Chromium is the Open Source base for Google Chrome, before Google puts some of their integrations and tracking into it. If you need Google Chrome for compatibility reasons, try Chromium first, it will probably work the same. It also comes in the distribution ChromiumOS (rather than ChromeOS).

<https://www.chromium.org/>