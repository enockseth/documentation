# Emacs
One of the two giants (other being [vim](./vim.md)) when it comes to powerful text editors. Has support for tons of plugins to improve the behavior.

<https://opensource.com/article/20/3/getting-started-emacs>

<http://www.jesshamrick.com/2012/09/10/absolute-beginners-guide-to-emacs/>

<https://www.gnu.org/software/emacs/>

<https://www.gnu.org/software/emacs/manual/>