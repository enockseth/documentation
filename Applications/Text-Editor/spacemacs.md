# Spacemacs
A text editor that borrows from both [emacs](./emacs.md) and [vim](./vim.md). 

<https://www.spacemacs.org/>