# Search
## DuckDuckGo
Default in Tor and Brave. Search Engine that works and doesn't store your searches. Not fully Open Source. Gets money from un-targeted ads at the top of results.

<https://duckduckgo.com/about>
## Searx
Open Source and can be self-hosted. May not have as quality of results as some of the other providers.

<https://searx.info/>
## StartPage
Also doesn't track you. Pays Google to use their search engine. Also earns its money from un-targeted ads ([citation](https://www.zdnet.com/article/startpage-private-search-engine-now-an-option-for-vivaldi-browser/)).

<https://www.startpage.com/>
## Ecosia
Ecosia's mission is to use the revenue generated by their ads and use them to plant trees. They proxy Bing search results. Has a minimal amount of tracking, but it respects [Do Not Track](https://en.wikipedia.org/wiki/Do_Not_Track) requests.

<https://www.ecosia.org/>