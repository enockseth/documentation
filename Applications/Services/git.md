# Git
## Gitlab
Hosted git. The server side code for Gitlab is open source, and you can [self-host](https://about.gitlab.com/install/). You can [migrate from Github](https://docs.gitlab.com/ee/user/project/import/github.html), and you can even login to Gitlab with your github account.

<https://about.gitlab.com/>
<https://gitlab.com>
## Github
This is more common to see than Gitlab, however their server side code is NOT Open Source, while they advocate for Open Source, their code itself isn't. Another consideration is Github is not owned by Microsoft.
<https://github.com>