# Distros

This is a distribution, or a specific configuration on top of the base GNU/Linux system. See [here](https://itsfoss.com/what-is-linux-distribution/) for more detail.

See [DistroWatch](https://distrowatch.com/) for a full list of Linux Distributions.
## Beginner

### Mint

Parent Distro: Ubuntu

Pros:

-   Built on Ubuntu

-   Doesn't have built-in shortcut to Amazon

Cons:

-   None

### Ubuntu

Parent Distro: Debian

Pros:

-   This is the most common user friendly choice for computers

Cons:

-   Has a built-in shortcut to Amazon's store

### Manjaro

Parent Distro: Arch

Pros:

-   Accessible to both newcomers and has extensive options for more experienced users

-   Easier to install than Arch

-   Built on Arch

Cons:

-   Not as user friendly as Ubuntu

### Pop OS

Parent Distro: Ubuntu

Pros:

-   Gaming

-   Made by system76

Cons:

-   Only has support for one desktop environment

## Intermediate

### OpenSUSE

Parent Distro: Independent Pros:

-   Some rare pieces of scientific software will focus on supporting openSUSE

Cons:

-   Few people use comparatively

-   Lack of Pros

### Debian

Parent Distro: Independent Pros:

-   Less bloat that would normally come with Ubuntu

-   Works on a Raspberry Pi

Cons:

-   Lack of Pros

### Fedora

Parent Distro: Independent Pros:

-   Less bloat

Cons:

-   Lack of Pros

-   Less user friendly

## Advanced

### CentOS

This is normally a server OS, yet it can also be used as a normal operating system as well. Parent Distro: Fedora Pros:

-   Made for servers

-   Stability

Cons:

-   Less user friendly

-   Older software (takes time to get feature updates)

### Arch

Parent Distro: Independent Pros:

-   Just what you need to run a system

-   Practically no bloat

-   Small

-   Newest software

Cons:

-   Less user friendly

-   Can be pain to install, as you do everything manually

-   Manjaro is easier to install

## Crazy


### Qubes OS

Parent Distro: Fedora

Everything in this distro runs in VMs, so you can run Fedora/Debian/Arch applications nearly seamlessly. Pros:

-   Security

-   Custom-ability

-   Multiple distros simultaneously, so you can handpick the distro for the job

Cons:

-   Can just break

-   Smaller project

-   Less support

-   Takes time and knowledge of how the VMs are arranged to setup properly

-   Does not run in a VM

-   Requires your CPU to have certain technologies

-   Takes a Lot of RAM

### Gentoo

You have to compile this yourself. Parent Distro: Independent Pros:

-   Just what you need to run a system

-   Practically no bloat

-   Can run on strange architectures

-   Can be installed beside Android

Cons:

-   Is a pain to install
