# Kubernetes
A tool to orchestrate many containers at once, designed for scale.

<https://kubernetes.io/>