# Partitions
## Partition/File System Types

### ext4

Moving files in ext4 is nearly instant. Can have files up to 16TB.

### fat32

This is type of the EFI partition. Can have files up to 4GB.

### swap

If you run out of RAM, your system may be able to use this space rather than crashing applications.

## Partition Tables

A partition table is a table that stores where each partition is on the disk. MSDOS is one such table, and it has a limit of 4 partitions. GPT is the current standard.