# Firewall
Generally a firewall allows finer grain control about the connections made in and out of your computer. For a normal computer, this normally means block all incoming connections, and allow outgoing. For servers, this normally means block all incoming except for these ports from these IPs.
## ufw
A simpler firewall manager. More information [here](https://wiki.archlinux.org/index.php/Uncomplicated_Firewall)
### gufw
Graphical Front end for the ufw firewall
1.  [Install](../Components/package-manager.md) gufw
2.  Open gufw
3.  Click on the status slider to turn it on.
4.  This will block other computers on the network from making
        connections into your machine.
## iptables
"traditional userspace utility for managing a firewall" - [Arch Wiki](https://wiki.archlinux.org/index.php/Category:Firewalls)

Some docs [here](https://phoenixnap.com/kb/iptables-tutorial-linux-firewall) [here](https://wiki.archlinux.org/index.php/Iptables) and [here](https://help.ubuntu.com/community/IptablesHowTo) for some explanation on how to use.
## Other
See more [here](https://wiki.archlinux.org/index.php/Category:Firewalls)