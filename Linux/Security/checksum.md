# Checksum
## Overview
A Checksum is a normally shorter value calculated from a larger input. For example a 500K file is reduced to simply `79922b4f359d2ad1281956ad878453a487d4476a1b08c315e99caf24c39a8e6a`. While there is some loss of data, it is still a valuable tool to check for modification, or see if two files are the same at a bit level. Any bit change will result in a fundementally different string. I changed one charecter in the file and it changed to `85eff5f1c6f53849ba696f02402f30468a18c269ff205b6048c782fe6a3f433d`. More Information [here](https://en.wikipedia.org/wiki/Checksum).
## Choosing an algorithm
There are various executables that preform this operation, some have been found to be insecure, see a list [here](https://en.wikipedia.org/wiki/Hash_function_security_summary) (try to avoid using red). At this time, sha256 is widely used, however sha512 is also out.
## Usage
`sha256sum FILE`