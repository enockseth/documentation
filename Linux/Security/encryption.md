# Encryption
## Public vs Private
## On Linux
<https://www.fosslinux.com/27018/best-ways-to-encrypt-files-in-linux.htm>
### Disk
Disk Encryption is useful when you are worried about someone stealing your physical drive. Once your computer is on, and you have provided the password, the Operating System (and all programs running), can see the decrypted version. So if you get a virus, or other malicious software, they can still read your files on an encrypted drive. However, if you drive is stolen, they will be unable to simply plug your drive into their computer and read everything. Disk Encryption makes it slightly harder to preform recovery, but not by much. If you forget your password, thats it. The files are locked, even from you.

<https://wiki.archlinux.org/index.php/Data-at-rest_encryption#Why_use_encryption?>
### GPG
### OpenSSH