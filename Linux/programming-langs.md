# Programming Languages
## Bash
<https://cs.lmu.edu/~ray/notes/bash/>
## Markdown
<https://www.markdownguide.org>
## Regex
<https://www.tutorialspoint.com/unix/unix-regular-expressions.htm>

<https://regexr.com/>