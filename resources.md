# Useful Resources
## Arch Wiki
Useful for a overview and medium detail explanation about configurations of Linux components as well as packages. Although package names vary from distro to distro, nearly all of the information transfer.

<https://wiki.archlinux.org/>
## Gentoo Wiki
Useful for a more technical lower level detail of Linux components.

<https://wiki.gentoo.org/wiki/Main_Page>
## Kali
<https://www.kali.org/docs/>
## LinuxDocs.org
<http://linuxdocs.org/>
## Debian Docs
<https://www.debian.org/doc/>
## Linux Foundation
<https://www.linuxfoundation.org/projects/linux/>
## Tutorials Point
<https://www.tutorialspoint.com/unix/index.htm>
## ibiblop.org
I collection of projects and operating systems with links to their wikis. May not be limited to Open Source, but does include them.

<https://www.ibiblio.org/catalog/items/browse>
## die.net
Has a cache of hundreds of manpages, as well as some basics

<https://linux.die.net/>
## The Linux Documentation Project
This is rather old, (most recent article dates from 2012), but may have some useful things

<https://tldp.org/>