# Remote Desktop Protocols
There are various remote desktop protocols, as well as several clients and servers for each protocol.
## VNC
VNC is the standard remote desktop solution for Linux, however it lacks some of the nice features like audio. tightvnc and tigervnc are two implementations of this protocol for Linux. There are countless other clients and servers for other platforms like MacOS, Android, Windows, even web ([novnc](https://novnc.com/)).
## SPICE
"The SPICE project aims to provide a complete open source solution for remote access to virtual machines in a seamless way so you can play videos, record audio, share usb devices and share folders without complications." ([source](https://www.spice-space.org/index.html)) It is integrated into QEMU.
## NX
This is [NoMachine's](https://www.nomachine.com) Protocol, it was open source up to version 2. See [X2Go](https://wiki.x2go.org/) for a maintained client/server. The NX protocol does not require nearly as much bandwidth compared to the other protocols.
## RDP
This is the Windows Remote Desktop protocol, but there are implementations for it on Linux, like freerdp. Nice thing about RDP is existing Windows integration, as well as a full feature set.