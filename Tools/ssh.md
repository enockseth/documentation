# SSH
SSH is a protocol to remotely execute commands on a target computer. Configured incorrectly it can be a huge security risk (not the fault of ssh, simply remote command execution is that dangerous).
## Configuration Tips
- Turn off root login
- Whitelist using iptables if you can
- Use SSH Keys and disable password login
## SSH Keys
- Generate a key with `ssh-keygen -b 4096 -t rsa -C YOUR_EMAIL_HERE`
- Add the contents of `~/.ssh/id_rsa.pub` on your computer to `~/.ssh/authorized_keys` on the remote computer.
  - Make sure to set the permissions on the remote computer correctly: `chmod 700 ~/.ssh` and `chmod 600 ~/.ssh/authorized_keys`
- Now you should be able to ssh in with `ssh USESR@HOST:22` (if no user is specified it assumes your own) (if no port is specified it assumes 22)
## SSH Config
SSH config is located at ~/.ssh/config. Used to save common hosts that you connect to so that you can connect easier.
```
Host SIMPLE_NAME
    HostName FULL_HOST_NAME_HERE
    User USER_NAME_HERE
    Port CUSTOM_PORT_HERE
```
(See [here](https://linuxize.com/post/using-the-ssh-config-file/) for more info)
## SSH Agent
- Start an SSH agent with `eval $(ssh-agent)` (some desktop environments start this for you)
- Add your ssh key `ssh-add PATH_TO_YOUR_KEY` (if no path is specified it assumes ~/.ssh/id_rsa)
- Now you will not need to be prompted for your password every time. If you want to forward your ssh agent to a remote server you can use the -A flag on the ssh command.
## More information
<https://wiki.archlinux.org/index.php/SSH_keys>