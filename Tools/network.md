# Networking Tools
## Connection
### curl/wget
Used for downloading files. Depending on how the website redirects, curl or wget may be easier to use.
### ping
Used to check to see if a host is up by sending [ICMP](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol) packets.
### traceroute
Shows the different connections established between you and the destination. Useful for debuging routing issues.
## DNS
### DIG
Prints out DNS records for a specified domain
## Packet Capture
### tcpdump
Dumps all TCP packets to output. Useful for a quick way to confirm that packets are indeed flowing.
### wireshark
Extremely useful tool for analyzing packet captures, does protocol parsing among other things.
## Ports
### nmap
Port scanner, can tell you what ports are open, as well as what protocol they are using.