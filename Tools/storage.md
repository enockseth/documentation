# Storage Tools
WARNING: DO NOT fill your entire drive on Linux, your system may FAIL to boot if it there is insufficient space. Keep in mind if your current user can't see a directory, these tools can't show you how much disk space those files are using.
## Tools to See What is Using Disk
### df
Disk Free simply tells you how much of each partition is used vs free
### du
Disk Usage lists all of the files in your current directory (and all sub folders)
### ncdu
Command line tool for viewing which directories (and all of their contents) are taking the most space. Much easier to follow than du. 
### boabab
Utility to show graphically how much space each folder is taking.